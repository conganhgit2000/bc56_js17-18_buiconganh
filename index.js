//cho người dùng nhập vào số muốn thêm vào mảng
var arrNumberN = [];

document.querySelector("#btn-nhap-so-n").onclick = function () {
  // input: người dùng nhập vào số n
  var nhapSoN = +document.querySelector("#nhap-so-n").value;
  // process: đẩy số mà người dùng nhập lên mảng arrNumberN
  arrNumberN.push(nhapSoN);
  // Hiển thị thông tin arrNumberN lên giao diện
  document.querySelector("#result").innerHTML = arrNumberN;
};

// Bài 1
document.querySelector("#btn-bt1").onclick = function () {
  // output: tổng của số dương
  var Tongbt1 = 0;
  // process: tính tổng số dương
  for (var index = 0; index < arrNumberN.length; index++) {
    if (arrNumberN[index] >= 0) {
      Tongbt1 += arrNumberN[index];
    }
  }
  //hiển thị kết quả lên giao diện
  document.querySelector("#result-bt1").innerHTML = "tổng: " + Tongbt1;
};

// Bài 2
document.querySelector("#btn-bt2").onclick = function () {
  // output: tổng của số dương sau khi đếm được
  var Tongbt2 = 0;
  // process: Đếm số dương trong mảng

  for (var index = 0; index < arrNumberN.length; index++) {
    if (arrNumberN[index] >= 0) {
      Tongbt2++;
    }
  }
  //hiển thị kết quả lên giao diện
  document.querySelector("#result-bt2").innerHTML = "tổng số dương: " + Tongbt2;
};

//Bài 3
document.querySelector("#btn-bt3").onclick = function () {
  // output: số nhỏ nhất
  var Tongbt3 = 0;
  // process: Tìm số nhỏ nhất
  var min = arrNumberN[0];
  var indexMin = 0;
  for (var index = 0; index < arrNumberN.length; index++) {
    if (arrNumberN[index] <= min) {
      min = arrNumberN[index];
      indexMin = index;
    }
    Tongbt3 = arrNumberN[indexMin];
  }
  //hiển thị kết quả lên giao diện
  document.querySelector("#result-bt3").innerHTML = "Số nhỏ nhất: " + Tongbt3;
};

// Bài 4
document.querySelector("#btn-bt4").onclick = function () {
  // output: Số dương nhỏ nhất
  var Tongbt4 = 0;
  
  var min = Infinity;
  var indexMin = 0;
  for (var index = 0; index < arrNumberN.length; index++) {
    if (arrNumberN[index] > 0 && arrNumberN[index] < min) {
      min = arrNumberN[index];
      indexMin = index;
    }
    Tongbt4 = arrNumberN[indexMin];
  }
  //hiển thị kết quả lên giao diện
  document.querySelector("#result-bt4").innerHTML = "Số nhỏ nhất: " + Tongbt4;
};

// Bài 5
document.querySelector("#btn-bt5").onclick = function () {
  // output: Số chẵn cuối cùng
  var ketQuaBt5 = 0;
  // process: Tìm số chẵn cuối cùng
  for (var index = arrNumberN.length; index >= 0; index--) {
    if (arrNumberN[index] % 2 === 0) {
      var lastIndex = index;
      break;
    }
  }
  ketQuaBt5 = arrNumberN[lastIndex];

  //hiển thị kết quả lên giao diện
  document.querySelector("#result-bt5").innerHTML =
    "Số chẵn cuối cùng: " + ketQuaBt5;
};

// Bài 6
document.querySelector("#btn-bt6").onclick = function () {
  var viTriSo1 = document.querySelector("#nhap-vi-tri-so-1").value * 1;
  var viTriSo2 = document.querySelector("#nhap-vi-tri-so-2").value * 1;
  // output: tổng của số dương
  var ketQuaBt6 = "";
  // process: Đổi vị trí
  //xác định thử là người dùng có nhập đúng vị trí hay không (có tồn tại hay không)
  if (
    viTriSo1 < 0 ||
    viTriSo1 > arrNumberN.length ||
    viTriSo2 < 0 ||
    viTriSo2 > arrNumberN.length
  ) {
    ketQuaBt6 = "Vị trí không hợp lệ";
  }

  // đổi vị trí theo người dùng yêu cầu
  var doiViTri1 = arrNumberN[viTriSo1];
  arrNumberN[viTriSo1] = arrNumberN[viTriSo2];
  arrNumberN[viTriSo2] = doiViTri1;
  ketQuaBt6 = arrNumberN;

  //hiển thị kết quả lên giao diện
  document.querySelector("#result-bt6").innerHTML =
    "Kết quả sau khi đổi vị trí: " + ketQuaBt6;
};

// Bài 7
document.querySelector("#btn-bt7").onclick = function () {
  // output: kết quả sau khi sắp xếp mảng
  var ketQuaBt7 = "";
  // process: Sắp xếp mảng theo thứ tự tăng dần
  arrNumberN.sort(function (a, b) {
    return a - b;
  });
  ketQuaBt7 = arrNumberN;
  //hiển thị kết quả lên giao diện
  document.querySelector("#result-bt7").innerHTML =
    "Mảng sau khi sắp xếp: " + ketQuaBt7;
};

// Bài 8

document.querySelector("#btn-bt8").onclick = function () {
  // output: số nguyên tố đầu tiên
  var ketQuabt8 = "";
  // process: Tìm số nguyên tố đầu tiên
  for (var index = 0; index < arrNumberN.length; index++) {
    var demUoc = 0;
    for (var i = 1; i <= arrNumberN[index]; i++) {
      if (arrNumberN[index] % i === 0) {
        demUoc++;
      }
    }
    if (demUoc == 2) {
      ketQuabt8 = "số nguyên tố đầu tiên là: " + arrNumberN[index];
      break;
    } else {
      // ketQuabt8 = "số nguyên tố không tồn tại";
      ketQuabt8 = "-1";
    }
  }

  //hiển thị kết quả lên giao diện
  document.querySelector("#result-bt8").innerHTML = ketQuabt8;
};

//Bài 9
var arrNewNumber = [];

document.querySelector("#btn-ts9").onclick = function () {
  // input: người dùng nhập vào
  var mangMoi = +document.querySelector("#nhap-mang").value;
  // process: đẩy số mà người dùng nhập lên mảng arrNewNumber
  arrNewNumber.push(mangMoi);
  // Hiển thị thông tin arrNewNumber lên giao diện
  document.querySelector("#result-ts9").innerHTML = "Mảng mới: " + arrNewNumber;
};

document.querySelector("#btn-bt9").onclick = function () {
  // output: Tổng tất cả các số nguyên của 2 mảng
  var ketQuabt9 = 0;
  var TongArrNumberN = 0;
  var TongArrNewNumber = 0;
  // process: Tìm Tổng số nguyên của mảng
  //tổng số nguyên của mảng thứ 1
  for (var index = 0; index < arrNumberN.length; index++) {
    var soNguyenArrNarrNumberN = Number.isInteger(arrNumberN[index]);
    if (soNguyenArrNarrNumberN == true) {
      TongArrNumberN++;
    }
  }
  //tổng số nguyên của mảng thứ 2
  for (var index = 0; index < arrNewNumber.length; index++) {
    var soNguyenArrNewNumber = Number.isInteger(arrNewNumber[index]);
    if (soNguyenArrNewNumber == true) {
      TongArrNewNumber++;
    }
  }
  ketQuabt9 = TongArrNumberN + TongArrNewNumber;
  //hiển thị kết quả lên giao diện
  document.querySelector("#result-bt9").innerHTML =
    "Tổng số nguyên: " + ketQuabt9;
};

// Bài 10
document.querySelector("#btn-bt10").onclick = function () {
  // output: kết quả sau khi sắp xếp mảng
  var ketQuaBt10 = "";
  var soDuong = 0;
  var soAm = 0;
  // process: Sắp xếp mảng theo thứ tự tăng dần
  //tìm tổng số nguyên dương
  for (var index = 0; index < arrNumberN.length; index++) {
    if (arrNumberN[index] > 0) {
      soDuong++;
    }
    if (arrNumberN[index] <= 0) {
      soAm++;
    }
  }
  if (soDuong > soAm) {
    ketQuaBt10 = "Số dương nhiều hơn số âm";
  } else if (soDuong < soAm) {
    ketQuaBt10 = "Số âm nhiều hơn số dương";
  } else {
    ketQuaBt10 = "Số âm và số dương bằng nhau";
  }
  //hiển thị kết quả lên giao diện
  document.querySelector("#result-bt10").innerHTML =
    "Tổng số dương: " +
    soDuong +
    ", số âm: " +
    soAm +
    " => Vì vậy " +
    ketQuaBt10;
};
